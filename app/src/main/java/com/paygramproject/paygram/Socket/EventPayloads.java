package com.paygramproject.paygram.Socket;

/**
 * Created by Sarath Kumar on 22-01-2016.
 */
public class EventPayloads{
    private Object objct;
    private String string;
    private String event;
    private int integer;

    public EventPayloads(String string, String event) {
        this.string = string;
        this.event = event;
    }

    public EventPayloads(int integer, String event) {
        this.integer = integer;
        this.event = event;
    }

    public EventPayloads(Object objct, String event) {
        this.objct = objct;
        this.event = event;
    }
    public Object getObjct() {
        return objct;
    }

    public String getString() {
        return string;
    }

    public String getEvent() {
        return event;
    }

    public int getInteger() {
        return integer;
    }
}
