package com.paygramproject.paygram.Socket;

/**
 * Created by Sarath Kumar on 22-01-2016.
 */
public class Events {
    public static String EVT_LOGOUT = "lgt";
    public static String EVT_NOITFY = "Notify";
    public static String EVT_JOIN = "join";
    public static String EVT_LEAVE= "lev";
    public static String EVT_JOIN_REPLY = "joinrpl";
    public static String EVT_WRITE_NFC = "Nfcwr";
    public static String EVT_VALIDATE_REPLY = "codereplay";
    public static String EVT_CODE_VALIDATE = "codeval";
    public static String EVT_GET_STORE = "gtstore";
    public static String EVT_GET_STORE_REPLY = "gtstorerp";
    public static String EVT_GET_USER = "gtusr";
    public static String EVT_GET_USER_REPLY = "gtstorerp";
    public static String EVT_PROCESS_PAYMENT = "payprocess";
    public static String EVT_PROCESS_PAYMENT_REPLAY = "payprocessrpl";
    public static String EVT_CHECKS = "checks";
    public static String EVT_ORDER_REPLY = "order_rpl";
    public static String EVT_CHECKS_REPLY = "checksrpl";
    public static String EVT_ORDER = "order";


}
