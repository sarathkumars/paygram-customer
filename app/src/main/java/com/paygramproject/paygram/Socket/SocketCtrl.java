package com.paygramproject.paygram.Socket;

import android.content.Context;
import android.util.Log;

import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Helper.Utils;
import com.squareup.otto.Bus;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;

/**
 * Created by Sarath Kumar on 22-01-2016.
 */
public class SocketCtrl {
    private Context context;
    private Socket socket;
    private static String TAG = "SOCKET CTRL";
    private static SocketCtrl Singleinstance = new SocketCtrl();
    private Bus bus;
    public boolean IsConnected = false;

    public Socket getSocket() {
        return socket;
    }

    public static SocketCtrl getInstance() {
        return Singleinstance;
    }

    public void init(final String token, final Context con, Bus busIns) throws URISyntaxException {
        this.context = con;
        this.bus = busIns;
        this.socket = IO.socket(Configs.getServer(con));
        this.context = con;
        socket.io().on(Manager.EVENT_TRANSPORT, new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        Transport transport = (Transport) args[0];
                        transport.on(Transport.EVENT_REQUEST_HEADERS, new Emitter.Listener() {
                            @Override
                            public void call(Object... args) {
                                @SuppressWarnings("unchecked")
                                Map<String, List<String>> headers = (Map<String, List<String>>) args[0];
                                headers.put("token", Arrays.asList(token));
                            }
                        });
                    }
                }

        );
        socket.connect();
        socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(TAG, "CONNECTED");
                IsConnected = true;
                bus.post("CONNECTED");
            }
        });
        socket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                bus.post("DISCONNECTED");
                IsConnected = false;
            }
        });
        socket.on(Socket.EVENT_RECONNECT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                bus.post("RECONNECT");
                IsConnected = true;

            }
        });

        socket.on(Socket.EVENT_RECONNECTING, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                bus.post("RECONNECTING");
            }
        });
        socket.on(Events.EVT_LOGOUT, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Utils.clearApplicationData(context);

                Log.i(TAG, "DONE LOG OUT");
            }
        });
        socket.on(Events.EVT_JOIN_REPLY, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                bus.post(new EventPayloads(args[0], Events.EVT_JOIN_REPLY));

            }
        });
        socket.on(Events.EVT_VALIDATE_REPLY, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                bus.post(new EventPayloads(args[0], Events.EVT_VALIDATE_REPLY));
            }
        });
        socket.on(Events.EVT_PROCESS_PAYMENT_REPLAY, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                bus.post(new EventPayloads(args[0], Events.EVT_PROCESS_PAYMENT_REPLAY));
            }
        });
        socket.on(Events.EVT_CHECKS_REPLY, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                 bus.post(new EventPayloads(args[0], Events.EVT_CHECKS_REPLY));

            }
        });
      socket.on(Events.EVT_ORDER_REPLY, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                 bus.post(new EventPayloads(args[0], Events.EVT_ORDER_REPLY));

            }
        });

    }

    public void EmitEvent(String Event, JSONObject payload) {
        if (socket != null) {
            try {
                socket.emit(Event, payload);

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }


    private SocketCtrl() {
    }
}
