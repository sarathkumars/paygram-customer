package com.paygramproject.paygram.Socket;

import com.squareup.otto.Bus;

/**
 * Created by Sarath Kumar on 22-01-2016.
 */
public class Businstance {
    private static Bus bus = new ThreadBus();
    public static Bus getInstance() {
        return bus;
    }
    private Businstance() {
    }
}
