package com.paygramproject.paygram.Socket;

import android.os.Handler;
import android.os.Looper;

import com.squareup.otto.Bus;

import java.util.ArrayList;

/**
 * Created by Sarath Kumar on 22-01-2016.
 */
public class ThreadBus extends Bus {
    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private ArrayList<Object> registeredObjects = new ArrayList<Object>();

    @Override
    public void register(Object object) {
        if (!registeredObjects.contains(object)) {
            registeredObjects.add(object);
            super.register(object);
        }
    }

    @Override
    public void unregister(Object object)
    {
        if (registeredObjects.contains(object)) {
            registeredObjects.remove(object);
            super.unregister(object);
        }
    }
    @Override
    public void post(final Object event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            super.post(event);
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    ThreadBus.super.post(event);
                }
            });
        }
    }
}
