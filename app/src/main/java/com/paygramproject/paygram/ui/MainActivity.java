package com.paygramproject.paygram.ui;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.util.LruCache;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Helper.Utils;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Realm.CustomerRealm;
import com.paygramproject.paygram.Realm.KeyValueStore;
import com.paygramproject.paygram.Socket.Businstance;
import com.paygramproject.paygram.Socket.EventPayloads;
import com.paygramproject.paygram.Socket.Events;
import com.paygramproject.paygram.Socket.SocketCtrl;
import com.paygramproject.paygram.ui.Scanners.QRScanner;
import com.paygramproject.paygram.ui.fragments.dashboard;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Arrays;

import butterknife.Bind;
import io.realm.Realm;
import retrofit.RequestInterceptor;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    RequestInterceptor requestInterceptor;
    private static final int REQUEST_CODE_ENABLE = 11;
    CircularImageView profilePic;
    TextView balance;
    String NavigationTitles[] = {"Wallet", "Payments", "Account", "Cards"};
    IconDrawable NavigationIcons[];
    String NavigationIconsStrign[] = {"{wallet}", "{lightning}", "{fa_user}", "{fa_credit_card}"};
    RecyclerView mRecyclerView;
    RecyclerView.LayoutManager mLayoutManager;
    DrawerLayout Drawer;
    FragmentTransaction fragmentTransaction;
    dashboard Dashboard_framgment;
    @Bind(R.id.nav_userName)
    TextView user;
    @Bind(R.id.nav_user_mail)
    TextView userEmail;
    Realm realm;
    LruCache<String, CustomerRealm> UserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Businstance.getInstance().register(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        fragmentTransaction = getFragmentManager().beginTransaction();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        LinearLayout layout = (LinearLayout) navigationView.getHeaderView(0);
        user = (TextView) layout.findViewById(R.id.nav_userName);
        userEmail = (TextView) layout.findViewById(R.id.nav_user_mail);
        profilePic = (CircularImageView) layout.findViewById(R.id.Userprofile);
        String secKEy = Utils.decryptString(Configs.ALIAS, Utils.Fileread(this, Configs.RECORDS_FILE), this);
        byte[] key = Arrays.copyOfRange(Base64.encode(secKEy.getBytes(), Base64.DEFAULT), 0, 64);
        realm = Utils.SecureRealm(this, key);
        CustomerRealm customer = realm.where(CustomerRealm.class).findFirst();
        SharedPreferences.Editor editor = getSharedPreferences(Configs.SHARED_PREF_USER, MODE_PRIVATE).edit();
        editor.putInt("uid", customer.getId());
        editor.putString("uname", customer.getName());
        editor.putString("uemail", customer.getEmail());
        editor.putString("propic", customer.getProfilepic());
        editor.putString("balance", customer.getProfilepic());
        editor.apply();
        user.setText(customer.getName());
        userEmail.setText(customer.getEmail());
        Picasso.with(this)
                .load(Configs.getServer(this) + "/profile/" + customer.getProfilepic())
                .into(profilePic);

        OkHttpClient client = new OkHttpClient();
          String token  = Utils.decryptString(Configs.ALIAS,Utils.Fileread(this,Configs.JWT_CRYPT),this);

        Request request = new Request.Builder()
                .url(Configs.getServer(this)+"api/customer/")
                        .get()
                        .addHeader("cache-control", "no-cache")
                        .addHeader("AcNtkn", token)
                        .build();
        if (Utils.isNetworkOnline(this))
        {
        try {
            Response response = client.newCall(request).execute();
            String JSONDATA = response.body().string();
            Log.i("UPDATE", JSONDATA);

            JSONObject object = new JSONObject(JSONDATA);
             if(!object.getString("updatedAt").equals(customer.getUpdatedAt())){
                Log.i("UPDATE","UPDATING USER");
                realm.beginTransaction();
                customer.setProfilepic(object.getString("profilepic"));
                customer.setAddress(object.getString("address"));
                customer.setName(object.getString("Name"));
                customer.setEmail(object.getString("Email"));
                customer.setBalance(object.getInt("Balance"));
                customer.setUpdatedAt(object.getString("updatedAt"));
                customer.setState(object.getString("State"));
                realm.commitTransaction();
             }

        } catch (Exception e) {
            e.printStackTrace();
        }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_dash) {


        } else if (id == R.id.nav_account) {

        } else if (id == R.id.nav_cards) {

        } else if (id == R.id.nav_wallet) {

        } else if (id == R.id.nav_payment) {

        } else if (id == R.id.nav_Logout) {

        } else if (id == R.id.nav_view) {

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Subscribe
    public void balanceRply(EventPayloads eventPayloads) {
        if (eventPayloads.getEvent().equals(Events.EVT_CHECKS_REPLY)) {

            JSONObject object = (JSONObject) eventPayloads.getObjct();
//            try {
//                balance.setText(object.getString("Balance"));
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (accessControl()) {
            if (Utils.isNetworkOnline(this)) {
                SocketCtrl socketCtrl = SocketCtrl.getInstance();
                if (!SocketCtrl.getInstance().IsConnected)
                {
                    try {
                        socketCtrl.init(Utils.decryptString(Configs.ALIAS, Utils.Fileread(this, Configs.JWT_CRYPT), this), this, Businstance.getInstance());
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
            }

        } else {
            Intent in = new Intent(MainActivity.this, LogIn.class);
            in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(in);
            finish();
        }

    }

    @Subscribe
    public void Connect(String Event) {
        Toast.makeText(this, Event, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(MainActivity.this, QRScanner.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private boolean accessControl() {

        try {
            realm = Utils.DefaultRealmConfigs(this);
            KeyValueStore store = realm.where(KeyValueStore.class)
                    .equalTo("Key", "frun")
                    .findFirst();
            if (store.getValue().equals("true")) {
                return true;

            } else {
                Toast.makeText(this, "  NULL", Toast.LENGTH_LONG).show();

                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


}
