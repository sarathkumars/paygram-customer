package com.paygramproject.paygram.ui.StoreFragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.d_codepages.sarathkumar.vectray.VectRay;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.paygramproject.paygram.Api.Retrofit_Interceptor;
import com.paygramproject.paygram.Api.StoreApi;
import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Models.PayToken;
import com.paygramproject.paygram.Models.Store;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Socket.Businstance;
import com.paygramproject.paygram.Socket.EventPayloads;
import com.paygramproject.paygram.Socket.Events;
import com.paygramproject.paygram.Socket.SocketCtrl;
import com.paygramproject.paygram.ui.StoreActivity;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Sarath Kumar on 08-03-2016.
 */
public class Connection_fragment extends Fragment {
    View MainView;
    TextView tx;
    PayToken token;
    CircularImageView Storeimage;
    TextView storeName, StoreType, info_msg;
    ImageView statusImage;
    Button btn_connect;
    VectRay vr;
    StoreActivity activity;
    boolean FromInstance = false;
    IconDrawable ic_cloud_off, cloud_Check, cloud;
    private  Store mStore;
    private int Connection_Status=0;
    public static int SATE_CONNECTED=1;
    public static int SATE_RESERVED=2;
    public static int SATE_OFFLINE=3;
/*TODO:FIX DISCONNECTION*/
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Businstance.getInstance().register(this);
        vr = new VectRay(getActivity(), getActivity().getPackageName());
        vr.LoadCustomTypeface("mat", "material.ttf");
        cloud = new IconDrawable(getActivity(), vr.get_icon("{mat} {cloud}"));
        cloud.sizeDp(120);
        cloud.color(Color.WHITE);
        ic_cloud_off = new IconDrawable(getActivity(), vr.get_icon("{mat} {ic_cloud_off}"));
        ic_cloud_off.sizeDp(120);
        ic_cloud_off.color(Color.WHITE);
        activity = (StoreActivity) getActivity();
        cloud_Check = new IconDrawable(getActivity(), vr.get_icon("{mat} {cloud_done}"));
        cloud_Check.sizeDp(120);
        cloud_Check.color(Color.WHITE);
        MainView = inflater.inflate(R.layout.fragment_connection, container, false);
        statusImage = (ImageView) MainView.findViewById(R.id.status_image);
        info_msg = (TextView) MainView.findViewById(R.id.info_msg);
        Storeimage = (CircularImageView) MainView.findViewById(R.id.StoreProfile);
        storeName = (TextView) MainView.findViewById(R.id.StoreName);
        btn_connect = (Button) MainView.findViewById(R.id.btn_connect);
        StoreType = (TextView) MainView.findViewById(R.id.Storetype);
        btn_connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Connection_Status==Connection_fragment.SATE_CONNECTED){
                    try {
                        SocketCtrl.getInstance()
                                .EmitEvent(Events.EVT_LEAVE
                                        , new JSONObject()
                                        .put("StoreID", token.getStoreId())
                                        .put("token", token.getTokenHASH())
                                        .put("tokenID", token.getId()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else{
                    btn_connect.setText("CONNECTING...");
                    try {
                        SocketCtrl.getInstance()
                                .EmitEvent(Events.EVT_JOIN
                                        , new JSONObject()
                                        .put("StoreID", token.getStoreId())
                                        .put("token", token.getTokenHASH())
                                        .put("tokenID", token.getId()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }


            }
        });
        if (token != null) {
            RestoreState();
        } else if (savedInstanceState != null) {
              token = savedInstanceState.getParcelable("token");
              mStore = savedInstanceState.getParcelable("store");
              Connection_Status = savedInstanceState.getInt("status");
            if (token != null) {
                if (token.getId() == token.getId()) {
                    RestoreState();
                }
            }

        } else {
            token = getActivity().getIntent().getParcelableExtra("Token");

            statusImage.setImageDrawable(cloud);
            initStoreView();
            if (!token.isTable_service()) {
                statusImage.setImageDrawable(ic_cloud_off);
                btn_connect.setVisibility(View.GONE);
                info_msg.setText("The service is not available right now.. you can pay Bills through billing Tab");
            } else {
                try {
                    Businstance.getInstance().post(new JSONObject().put("CODE", StoreActivity.HIDE_VIEWPAGER));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                btn_connect.setVisibility(View.VISIBLE);

            }


        }

        return MainView;

    }

    public void RestoreState() {

        try {
            Businstance.getInstance().post(new JSONObject().put("CODE", StoreActivity.SHOW_VIEWPAGER));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(Connection_Status==Connection_fragment.SATE_RESERVED){


            info_msg.setText("This token is current Used by someone Else");
            btn_connect.setVisibility(View.VISIBLE);
            statusImage.setImageDrawable(ic_cloud_off);
            btn_connect.setText("Retry Connection");
            Log.i("DESK SERVIC", "INSTANCE RESERVED STATE");
            StoreActivity activity = (StoreActivity) getActivity();
            activity.viewPager.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
            Picasso.with(getActivity())
                    .load(Configs.getServer(getActivity()) + "/store/" + mStore.getStorePic())
                    .resize(200, 200)
                    .centerCrop()
                    .into(Storeimage);
        }else if(Connection_Status==Connection_fragment.SATE_CONNECTED)
        {

            info_msg.setText("You're now Connected to "+mStore.getStoreName());
            btn_connect.setVisibility(View.VISIBLE);
            statusImage.setImageDrawable(cloud_Check);
            btn_connect.setText("Disconnect Connection");
            btn_connect.setClickable(true);
            Log.i("DESK SERVIC", "INSTANCE SUCCESS STATE");
            Picasso.with(getActivity())
                    .load(Configs.getServer(getActivity()) + "/store/" + mStore.getStorePic())
                    .resize(200, 200)
                    .centerCrop()
                    .into(Storeimage);

        }else if(Connection_Status==Connection_fragment.SATE_OFFLINE){
            statusImage.setImageDrawable(ic_cloud_off);
            btn_connect.setVisibility(View.GONE);
            info_msg.setText("This Store Service is Now Offline");
            Log.i("DESK SERVIC", "INSTANCE OFFLINE STATE");
            Picasso.with(getActivity())
                    .load(Configs.getServer(getActivity()) + "/store/" + mStore.getStorePic())
                    .resize(200, 200)
                    .centerCrop()
                    .into(Storeimage);

        }

    }


    @Subscribe
    public void EventReplays(EventPayloads EventReply) {
        if (EventReply.getEvent().equals(Events.EVT_JOIN_REPLY)) {
            JSONObject Object = (JSONObject) EventReply.getObjct();
            Log.i("DESK SERVIC",Object.toString());

            try {
                if (Object.getString("status").equals("reserved")) {

                    info_msg.setText("This token is current Used by someone Else");
                    btn_connect.setVisibility(View.VISIBLE);
                    statusImage.setImageDrawable(ic_cloud_off);
                    btn_connect.setText("Retry Connection");
                    Connection_Status = Connection_fragment.SATE_RESERVED;
                    Log.i("DESK SERVIC","RESERVED STATE");

                }else if (Object.getString("status").equals("success")) {

                    info_msg.setText("Yore now Connected to "+mStore.getStoreName());
                    btn_connect.setVisibility(View.VISIBLE);
                    statusImage.setImageDrawable(cloud_Check);
                    btn_connect.setText("Disconnect Connection");
                    btn_connect.setClickable(true);
                    Log.i("DESK SERVIC", "success STATE");
                    Connection_Status = Connection_fragment.SATE_CONNECTED;
                    try {
                        Businstance.getInstance().post(new JSONObject().put("CODE", StoreActivity.SHOW_VIEWPAGER));
                        Businstance.getInstance().post(token);
                        activity.Putinstance(new JSONObject().put("token", token));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }else if (Object.getString("status").equals("offline")) {

                    statusImage.setImageDrawable(ic_cloud_off);
                    btn_connect.setVisibility(View.GONE);
                    info_msg.setText("This Store Service is Now Offline");
                    Connection_Status = Connection_fragment.SATE_OFFLINE;
                    Log.i("DESK SERVIC", "offline STATE");


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null) {
            token = savedInstanceState.getParcelable("token");
            mStore = savedInstanceState.getParcelable("store");
            Connection_Status = savedInstanceState.getInt("status");
        }
        super.onViewStateRestored(savedInstanceState);
    }

     @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("token", token);
         outState.putInt("status", Connection_Status);
         outState.putParcelable("store", mStore);
    }


    public void initStoreView() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Configs.getServer(getActivity()))
                .setRequestInterceptor(Retrofit_Interceptor.getInstance(getActivity()))
                .build();
        StoreApi Storeapi = adapter.create(StoreApi.class);
        Storeapi.getStore(token.getStoreId(), new Callback<Store>() {
            @Override
            public void success(Store store, Response response) {
                mStore = store;
                if (!FromInstance) {
                    btn_connect.setText("Connect To " + store.getStoreName());
                    Log.i("CONNECTION","FROMINSTA");
                }
                Picasso.with(getActivity())
                        .load(Configs.getServer(getActivity()) + "/store/" + store.getStorePic())
                        .resize(200, 200)
                        .centerCrop()
                        .into(Storeimage);
                storeName.setText(store.getStoreName());
                StoreType.setText(store.getStoreTypes());
                btn_connect.setClickable(true);


            }

            @Override
            public void failure(RetrofitError error) {
                Log.i("GOT DATA,", error.getMessage());

            }
        });

    }

    @Override
    public void onDestroy() {
        if(Connection_Status==Connection_fragment.SATE_CONNECTED) {
            try {
                SocketCtrl.getInstance()
                        .EmitEvent(Events.EVT_LEAVE
                                , new JSONObject()
                                .put("StoreID", token.getStoreId())
                                .put("token", token.getTokenHASH())
                                .put("tokenID", token.getId()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        super.onDestroy();
    }

}