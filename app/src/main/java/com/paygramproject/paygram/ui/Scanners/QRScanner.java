package com.paygramproject.paygram.ui.Scanners;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.d_codepages.sarathkumar.vectray.VectRay;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Helper.JSON_utils;
import com.paygramproject.paygram.Models.PayToken;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Socket.Businstance;
import com.paygramproject.paygram.Socket.EventPayloads;
import com.paygramproject.paygram.Socket.Events;
import com.paygramproject.paygram.Socket.SocketCtrl;
import com.paygramproject.paygram.ui.PaymentProcessor;
import com.paygramproject.paygram.ui.StoreActivity;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;
import uk.me.lewisdeane.ldialogs.BaseDialog;
import uk.me.lewisdeane.ldialogs.CustomDialog;

/**
 * Created by Sarath Kumar on 17-01-2016.
 */
public class QRScanner extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    private String lastcode = null;
    private static String TAG = "QRCAMERA";
    private MediaPlayer mediaPlayer;
    FloatingActionButton fab;
    FrameLayout InfoCardFrame;
    VectRay vr;
    boolean IsInflated = false;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        vr = new VectRay(this, getPackageName());
        vr.LoadCustomTypeface("app", "app.ttf");
        setContentView(R.layout.activity_qrscanner);
        mediaPlayer = MediaPlayer.create(QRScanner.this, R.raw.beep);
        ViewGroup contentFrame = (ViewGroup) findViewById(R.id.content_frame);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        InfoCardFrame = (FrameLayout) findViewById(R.id.MainFrame);
        final IconDrawable fabdrw = new IconDrawable(this, vr.get_icon("{fa} {fa_times}"));
        fabdrw.color(Color.WHITE);
        fab.setImageDrawable(fabdrw);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (IsInflated) {
                    InfoCardFrame.removeAllViews();
                    IsInflated = false;
                    mScannerView.stopCamera();
                    mScannerView.setResultHandler(QRScanner.this);
                    mScannerView.startCamera();
                    lastcode = null;
                    fab.setImageDrawable(fabdrw);
                } else {
                    finish();
                }
            }
        });
        mScannerView = new ZXingScannerView(this) {
            @Override
            protected IViewFinder createViewFinderView(Context context) {
                return new CustomViewFinderView(context);
            }
        };
        contentFrame.addView(mScannerView);
        List<BarcodeFormat> format = new ArrayList();
        format.add(BarcodeFormat.QR_CODE);
        mScannerView.setFormats(format);
        Businstance.getInstance().register(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mScannerView.stopCamera();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }

    @Subscribe
    public void ValidationCallback(EventPayloads event) {
 /*Event callback when successful token found*/
        Log.i(TAG, "VALIDATE CALLBACK CALLED");
        if (event.getEvent().equals(Events.EVT_JOIN_REPLY)) {
            JSONObject object = (JSONObject) (event.getObjct());

            Log.i(TAG, "GOT JOIN REPLAY");
            try {
                Log.i(TAG, String.valueOf(object.getBoolean("payload")));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (event.getEvent().equals(Events.EVT_VALIDATE_REPLY)) {
          /*  getting JSON object and parsing for validation*/
            JSONObject object = (JSONObject) (event.getObjct());
            PayToken payToken = null;
            try {
                payToken = JSON_utils.payment_Token_Parser(object);
                Log.i(TAG, payToken.getTokenHASH());
            } catch (JSONException e) {
                e.printStackTrace();
                Log.i(TAG, "JSON EXCP" + e.getMessage());
            }

            if (payToken != null) {
                Log.i(TAG, "TOKEN NULL");

                if (payToken.isvalid()) {
                    /*token is valid */
                    Log.i(TAG, "VALID TOKEN");
                  /*  checking whether token is Dynamic or Static.because both require Expiration*/
                    if (payToken.getTokentype().equals("Dynamic") || payToken.getTokentype().equals("Static")) {
                        Log.i(TAG, "DYNIMIC || STATIC");
                  /* getting current UNIX timestamp and comparing with token Expire value*/
                        Long tsLong = System.currentTimeMillis();
                        Log.i(TAG, "TIME:" + tsLong);
                        Log.i(TAG, "EXP:" + payToken.getExpire());
                        if (payToken.getStoreId() != 0) {
                            infoCardInfalator(payToken);
                        } else if (payToken.ispaid()) {
                            /*checking the token is paid or not*/
                            Log.i(TAG, "ALREADY PAID");

                            Snackbar snackbar = Snackbar
                                    .make(fab, "This Token is already paid!...", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        } else if (tsLong > payToken.getExpire()) {
                       /*     if current timestamp is less than token timestamp that means the token is expired
                            and a snackbar msg will be shown to user*/
                            Log.i(TAG, "EXPIRED");
                            Snackbar snackbar = Snackbar
                                    .make(fab, "Token Expire!...", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        } else {
                          /*  at this point the token validation is complete and proceeding to show token info to the user*/
                            Log.i(TAG, "EVERYTHING GOOD");
                            infoCardInfalator(payToken);

                        }

                    } else if (payToken.getTokentype().equals("Donate")) {
                        Log.i(TAG, "DONATION SCHEME");
                        /*found Donation token this require additional user input from the user*/
                    }

                } else {
                    /*showing invalid token in scanview*/
                    Log.i(TAG, "NON VALID TOKEN");
                    Snackbar snackbar = Snackbar
                            .make(fab, "Invalid Token please try again!...", Snackbar.LENGTH_LONG);
                    snackbar.show();
                }
            }
        }
    }

    private void infoCardInfalator(final PayToken payToken) {
         /*inflator  service for inflating info cardview*/
        LayoutInflater inflater = (LayoutInflater) QRScanner.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         /*inflating layout from resource*/
        final RelativeLayout validateview = (RelativeLayout) inflater.inflate(R.layout.part_validation_card, null);

        CircularImageView Payee = (CircularImageView) validateview.findViewById(R.id.payeeProf);
        final TextView Payee_name = (TextView) validateview.findViewById(R.id.merchantName);
        Button Paybutton = (Button) validateview.findViewById(R.id.Paybutton);
        Button visit = (Button) validateview.findViewById(R.id.visit);
        TextView Amount = (TextView) validateview.findViewById(R.id.Amount);
        visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SocketCtrl.getInstance().IsConnected)
                {
                    Intent in = new Intent(QRScanner.this, StoreActivity.class);
                    validateview.setVisibility(View.GONE);
                    in.putExtra("Token", payToken);
                    startActivity(in);
                }
            }
        });

        Paybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent PaymentProccess = new Intent(QRScanner.this, PaymentProcessor.class);
                PaymentProccess.putExtra("Token", payToken);
                validateview.setVisibility(View.GONE);
                startActivity(PaymentProccess);

            }
        });
        String profile_URL = "/store/" + payToken.getProfile();
        if (payToken.getStoreId() == 0)
        {
            visit.setVisibility(View.GONE);
            ViewGroup.LayoutParams params = Paybutton.getLayoutParams();
            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            Paybutton.setLayoutParams(params);
            profile_URL = "/profile/" + payToken.getProfile();

        } else {
            Amount.setVisibility(View.GONE);
            ViewGroup.LayoutParams params = Paybutton.getLayoutParams();
            params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
            Paybutton.setLayoutParams(params);
            visit.setVisibility(View.VISIBLE);
        }
        Picasso.with(this)
                .load(Configs.getServer(this) + profile_URL)
                .resize(150, 150)
                .centerCrop()
                .into(Payee);
        Amount.setText("Rs." + payToken.getAmount());
        Payee_name.setText(payToken.getMerchantName());
        validateview.startAnimation(AnimationUtils.loadAnimation(QRScanner.this, R.anim.push_up_in));
        InfoCardFrame.removeAllViews();
        InfoCardFrame.addView(validateview);
        IconDrawable fabdrw = new IconDrawable(this, vr.get_icon("{app} {reload}"));
        fabdrw.color(Color.WHITE);
        fab.setImageDrawable(fabdrw);
        IsInflated = true;


    }


    public void DialogueMaker(String content) {
        CustomDialog.Builder builder = new CustomDialog.Builder(this, "Not Valid", "Ok");
        builder.content(content);
        builder.contentAlignment(BaseDialog.Alignment.CENTER);
        builder.darkTheme(false);
        builder.build();
        CustomDialog customDialog = builder.build();
        customDialog.show();
        customDialog.setClickListener(new CustomDialog.ClickListener() {
            @Override
            public void onConfirmClick() {
                mScannerView.startCamera();
            }

            @Override
            public void onCancelClick() {

            }
        });

    }

    @Override
    public void handleResult(Result rawResult) {
        Log.i(TAG, "RESULT GOT");
        mScannerView.resumeCameraPreview(this);
        if (lastcode == null || !lastcode.equals(rawResult.getText())) {
            Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibe.vibrate(100);
            mediaPlayer.start();
            JSONObject object = new JSONObject();
            try {
                object.put("code", rawResult.getText());
                Log.i(TAG, "JSON OK");

            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.i(TAG, "EMIT OK");

            SocketCtrl.getInstance().EmitEvent(Events.EVT_CODE_VALIDATE, object);
            lastcode = rawResult.getText();

        }

    }

    private static class CustomViewFinderView extends ViewFinderView {
        public static final int TRADE_MARK_TEXT_SIZE_SP = 40;
        public final Paint PAINT = new Paint();

        public CustomViewFinderView(Context context) {
            super(context);
        }

        public CustomViewFinderView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }


        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
        }

    }
}



