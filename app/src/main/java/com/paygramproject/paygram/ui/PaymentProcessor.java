package com.paygramproject.paygram.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.d_codepages.sarathkumar.vectray.VectRay;
import com.d_codepages.sarathkumar.vectray.VectrayDrawable;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.paygramproject.paygram.Api.CustomerApi;
import com.paygramproject.paygram.Api.Retrofit_Interceptor;
import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Helper.Utils;
import com.paygramproject.paygram.Models.Customer;
import com.paygramproject.paygram.Models.PayToken;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Socket.Businstance;
import com.paygramproject.paygram.Socket.EventPayloads;
import com.paygramproject.paygram.Socket.Events;
import com.paygramproject.paygram.Socket.SocketCtrl;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class PaymentProcessor extends AppCompatActivity {
    public static String TAG = PaymentProcessor.class.getSimpleName();
    private PayToken payToken;
    @VectrayDrawable(Icon = "{app} {timer}", Color = Color.WHITE, Dp = 120)
    public IconDrawable timer;
    @VectrayDrawable(Icon = "{fa} {fa_user}", Color = Color.WHITE, Dp = 100)
    public IconDrawable user;
    @VectrayDrawable(Icon = "{app} {wallet}", Color = Color.WHITE, Dp = 120)
    public IconDrawable wallet;
    @VectrayDrawable(Icon = "{fa} {fa_inr}", Color = Color.GREEN, Dp = 25)
    public IconDrawable inr;
    @VectrayDrawable(Icon = "{app} {check}", Color = Color.GREEN, Dp = 120)
    public IconDrawable Check;

    @VectrayDrawable(Icon = "{fa} {fa_exclamation_triangle}", Color = Color.RED, Dp = 120)
    public IconDrawable Error;
    TextView infoText, name, amount,afterbalance,currentbalance,amountshrot;
    ImageView infoicon, inrimg;
    Button Paybutton;
    ProgressBar progress_spinner;
    private boolean REPLY_RECIEVED = false;

    private timeoutTask task;
    Timer timer_task;

    AppBarLayout Appbar;
    CircularImageView payeeprof;
    FrameLayout wrapper;
    private static int REQUEST_LOCK = 52894;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        VectRay vr = new VectRay(this, getPackageName());
        vr.LoadCustomTypeface("app", "app.ttf");
        setContentView(R.layout.activity_payment_processor);
        infoicon = (ImageView) findViewById(R.id.infoicon);
        infoText = (TextView) findViewById(R.id.infoText);
        Appbar = (AppBarLayout) findViewById(R.id.Appbar);
        payeeprof = (CircularImageView) findViewById(R.id.payeeProf);
        name = (TextView) findViewById(R.id.name);
        progress_spinner = (ProgressBar) findViewById(R.id.progress_spinner);
        amount = (TextView) findViewById(R.id.payableAmount);
        Paybutton = (Button) findViewById(R.id.Paybutton);
        amountshrot = (TextView) findViewById(R.id.amount);
        afterbalance = (TextView) findViewById(R.id.afterbalance);
        currentbalance = (TextView) findViewById(R.id.currentbalance);
        Businstance.getInstance().register(this);
        Appbar.setVisibility(View.GONE);
        progress_spinner.setVisibility(View.VISIBLE);
        if(!SocketCtrl.getInstance().IsConnected){
            try {
                SocketCtrl.getInstance().init(Utils.decryptString(Configs.ALIAS,Utils.Fileread(this,Configs.JWT_CRYPT),this),this, Businstance.getInstance());
            } catch (URISyntaxException e) {
                 finish();
            }
        }
        try {
            payToken = getIntent().getParcelableExtra("Token");
            Log.i(TAG,getIntent().getParcelableExtra("Token").toString());

        } catch (Exception e) {
            e.printStackTrace();
            finish();
        }
        Long tsLong = System.currentTimeMillis();
        if (tsLong > payToken.getExpire()) {
            /*Expired token*/
            progress_spinner.setVisibility(View.GONE);
            Appbar.setVisibility(View.GONE);
            infoText.setVisibility(View.VISIBLE);
            infoicon.setVisibility(View.VISIBLE);
            infoicon.setImageDrawable(timer);
            infoText.setText("Token Expired");
        }else{
            RestAdapter adapter = new RestAdapter.Builder()
                    .setEndpoint(Configs.getServer(this))
                    .setRequestInterceptor(Retrofit_Interceptor.getInstance(this))
                    .build();
            CustomerApi customer = adapter.create(CustomerApi.class);
            customer.getUser(new Callback<Customer>() {
                @Override
                public void success(Customer customer, Response response) {
                    if (customer.getBalance() < payToken.getAmount()) {
                        SetinsuffientBalance();
                        Log.i(TAG, "" + customer.getBalance());
                    } else {
                        SetSummeryView(customer);

                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }


    }

    private void SetSummeryView(Customer customer) {
         if (payToken.getProfile() == null)
         {
            payeeprof.setImageDrawable(user);
        } else {
            String profile_URL = "/store/" + payToken.getProfile();
            if (payToken.getStoreId() == 0) {
                profile_URL = "/profile/" + payToken.getProfile();
            }
            Picasso.with(this)
                    .load(Configs.SERVER + profile_URL)
                    .fit()
                    .into(payeeprof);


        }
        amount.setText("Rs." + payToken.getAmount());
        name.setText(payToken.getMerchantName());
        amountshrot.setText("Rs." + payToken.getAmount());
        currentbalance.setText("Rs."+customer.getBalance());
        afterbalance.setText("Rs." + (customer.getBalance() - payToken.getAmount()));
        Paybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertbox = new AlertDialog.Builder(PaymentProcessor.this);
                alertbox.setTitle("Alert");
                alertbox.setMessage("Are you sure to Continue");
                alertbox.setPositiveButton("Go On", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(PaymentProcessor.this, Locker.class);
                        intent.putExtra("REQ", "5445445sdsdsds");
                        startActivityForResult(intent, REQUEST_LOCK);
                    }
                });
                alertbox.setNegativeButton("Nop", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alertbox.show();


            }
        });

        Animation in = AnimationUtils.loadAnimation(this, R.anim.push_right_in);
        progress_spinner.setVisibility(View.GONE);
        Appbar.setVisibility(View.VISIBLE);
        Appbar.startAnimation(in);

    }

    private void SetinsuffientBalance() {
        progress_spinner.setVisibility(View.GONE);
        Appbar.setVisibility(View.GONE);
        infoText.setVisibility(View.VISIBLE);
        infoicon.setVisibility(View.VISIBLE);
        infoicon.setImageDrawable(wallet);
        infoText.setText("Insufficient Balance");
    }

    @Subscribe
    public  void validatecallback(EventPayloads event){
        REPLY_RECIEVED =true;
         if (event.getEvent().equals(Events.EVT_PROCESS_PAYMENT_REPLAY)) {
            Log.i(TAG,"CALLED PAY COMPLETE");
            JSONObject object = (JSONObject) (event.getObjct());
             Log.i(TAG,event.getObjct().toString());
            try {
                Log.i(TAG,object.getString("status"));

                if(object.getString("status").equals("SUCCESS"))
                {
                    Log.i(TAG, "EVT SUCCESS");
                    Animation out = AnimationUtils.loadAnimation(this, R.anim.push_left_out);
                            Appbar.startAnimation(out);
                    out.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }
                        @Override
                        public void onAnimationEnd(Animation animation) {
                            progress_spinner.setVisibility(View.GONE);
                            infoicon.setVisibility(View.VISIBLE);
                            infoText.setVisibility(View.VISIBLE);
                            infoicon.startAnimation(AnimationUtils.loadAnimation(PaymentProcessor.this, R.anim.push_left_in));
                            Check.color(getResources().getColor(R.color.Emrald_green));
                            infoicon.setImageDrawable(Check);
                            infoText.setText("Payment Success");
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                }else{
                    progress_spinner.setVisibility(View.GONE);
                    infoicon.setVisibility(View.VISIBLE);
                    infoText.setVisibility(View.VISIBLE);
                    infoicon.startAnimation(AnimationUtils.loadAnimation(PaymentProcessor.this, R.anim.push_left_in));
                    Error.color(getResources().getColor(R.color.flat_red));
                    infoicon.setImageDrawable(Error);
                    if(object.getString("message").equals("INSUFFICIENT BALANCE"))
                    {
                        infoText.setText("Insuffient Balance");
                        infoicon.setImageDrawable(wallet);

                    }else {
                        infoicon.setImageDrawable(Error);
                        infoText.setText("Something Went Wrong");

                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_LOCK) {
            if (resultCode == RESULT_OK) {
                if("5445445sdsdsds".equals(data.getStringExtra("REQ"))){
                    boolean STATE = data.getBooleanExtra("success", false);
                    if(STATE){
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("token", payToken.getTokenHASH());
                            jsonObject.put("Tokentype",payToken.getTokentype());
                            Log.i(TAG, payToken.toString());
                            SocketCtrl.getInstance().EmitEvent(Events.EVT_PROCESS_PAYMENT, jsonObject);
                            timer_task = new Timer();
                            task = new timeoutTask();
                            timer_task.schedule(task, 5000);
                            progress_spinner.setVisibility(View.VISIBLE);
                            Appbar.setVisibility(View.GONE);
                        } catch (JSONException e ) {
                            e.printStackTrace();
                        }

                    }

                }

            }
        }
    }

    class timeoutTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(!REPLY_RECIEVED){
                        progress_spinner.setVisibility(View.GONE);
                        infoicon.setVisibility(View.VISIBLE);
                        infoText.setVisibility(View.VISIBLE);
                        infoicon.startAnimation(AnimationUtils.loadAnimation(PaymentProcessor.this, R.anim.push_left_in));
                        Error.color(getResources().getColor(R.color.flat_red));
                        infoicon.setImageDrawable(Error);
                        infoText.setText("Something Went Wrong");
                    }
                }
            });
        }

    }
}
