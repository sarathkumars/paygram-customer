package com.paygramproject.paygram.ui.StoreFragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paygramproject.paygram.Api.Retrofit_Interceptor;
import com.paygramproject.paygram.Api.StoreApi;
import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Models.PayToken;
import com.paygramproject.paygram.Models.product;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Socket.Businstance;
import com.paygramproject.paygram.Socket.EventPayloads;
import com.paygramproject.paygram.Socket.Events;
import com.paygramproject.paygram.ui.StoreActivity;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Sarath Kumar on 06-02-2016.
 */
public class products_fragment extends Fragment {

    private static final String KEY_LAYOUT_MANAGER = "layoutManager";
    private static final int SPAN_COUNT = 2;

    private enum LayoutManagerType {
        GRID_LAYOUT_MANAGER,
        LINEAR_LAYOUT_MANAGER
    }
    HashMap<Integer, Integer> AdapterItemLocation = new HashMap<Integer, Integer>();

    private String TAG = this.getClass().getSimpleName();


    protected LayoutManagerType mCurrentLayoutManagerType;
    protected boolean LoadComplete = false;


    protected RecyclerView mRecyclerView;
    PayToken mtoken = null;
    protected Adapter_Products mAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    protected List<product> mDataset =null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Businstance.getInstance().register(this);
        setRetainInstance(true);
        PayToken token = getActivity().getIntent().getParcelableExtra("Token");
        if (mtoken != null) {
            initDataset();
        } else {
            mtoken = token;
            if (mtoken != null)
            {
                initDataset();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_products, container, false);
        rootView.setTag(TAG);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.RecyCler_ProductFragment);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        if (savedInstanceState != null)
        {
            mCurrentLayoutManagerType = (LayoutManagerType) savedInstanceState
                    .getSerializable(KEY_LAYOUT_MANAGER);
        }
        if(mDataset!=null){
            mAdapter = new Adapter_Products(mDataset, getActivity(), mtoken);
            mRecyclerView.setAdapter(mAdapter);
        }
        setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
        return rootView;
    }

    @Subscribe
    public void EventReplays(PayToken token) {
        if (mtoken != null) {
            initDataset();
        } else {
            mtoken = token;
            if (mtoken != null) {
                initDataset();
            }
        }
    }
    @Subscribe
    public void Events(EventPayloads payload) {
        if (payload.getEvent().equals(Events.EVT_ORDER_REPLY)) {
            JSONObject object = (JSONObject) payload.getObjct();
            try {
                String orderSattus= object.getString("orderStatus");
                int productID =  object.getInt("ID");
                int location = AdapterItemLocation.get(productID);
                if(orderSattus.equals("SUCCESS")){
                    mDataset.get(location).setOrderStatus(product.ORDER_STATUS_SUCCESS);
                    mAdapter.notifyItemChanged(location);
                }else if(orderSattus.equals("FAIL")){
                    mDataset.get(location).setOrderStatus(product.ORDER_STATUS_FAILED);
                    mAdapter.notifyItemChanged(location);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
    public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
        int scrollPosition = 0;
        if (mRecyclerView.getLayoutManager() != null) {
            scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
                    .findFirstCompletelyVisibleItemPosition();
        }
        switch (layoutManagerType) {
            case GRID_LAYOUT_MANAGER:
                mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
                mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
                break;
            case LINEAR_LAYOUT_MANAGER:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
                break;
            default:
                mLayoutManager = new LinearLayoutManager(getActivity());
                mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
        }

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.scrollToPosition(scrollPosition);
    }


    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState!=null){
            mDataset = savedInstanceState.getParcelableArrayList("PRODUCTS");
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        savedInstanceState.putParcelableArrayList("PRODUCTS", (ArrayList<? extends Parcelable>) mDataset);
        savedInstanceState.putSerializable(KEY_LAYOUT_MANAGER, mCurrentLayoutManagerType);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("TEST", "DESTRYED");
    }

    private void initDataset() {
        mDataset = new ArrayList<>();
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(Configs.getServer(getActivity()))
                .setRequestInterceptor(Retrofit_Interceptor.getInstance(getActivity()))
                .build();
        StoreApi Storeapi = adapter.create(StoreApi.class);
        Storeapi.getProducts(mtoken.getStoreId(), new Callback<List<product>>() {
            @Override
            public void success(List<product> products, Response response) {
                LoadComplete = true;
                mDataset = products;
                StoreActivity BaseActivity = (StoreActivity) getActivity();
                BaseActivity.setProducts(products);
                Log.i(TAG, "GOT DATA " + products.size());
                mAdapter = new Adapter_Products(mDataset, getActivity(), mtoken);
                mRecyclerView.setAdapter(mAdapter);
                MapViewLocationBasedAdapter();

            }

            @Override
            public void failure(RetrofitError error) {
                Log.i(TAG + " ERROR", error.getMessage());
                LoadComplete = true;
                mDataset = null;
            }
        });

     }


    private void MapViewLocationBasedAdapter() {
        int i = 0;
        while (i < mDataset.size()) {
            AdapterItemLocation.put(mDataset.get(i).getId(), i);
            i++;
        }
        ;
    }
}


