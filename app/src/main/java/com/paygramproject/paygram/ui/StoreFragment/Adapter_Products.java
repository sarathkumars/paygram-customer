package com.paygramproject.paygram.ui.StoreFragment;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.d_codepages.sarathkumar.vectray.VectRay;
import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Models.PayToken;
import com.paygramproject.paygram.Models.product;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Socket.Businstance;
import com.paygramproject.paygram.Socket.EventPayloads;
import com.paygramproject.paygram.Socket.Events;
import com.paygramproject.paygram.Socket.SocketCtrl;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Sarath Kumar on 13-03-2016.
 */
public class Adapter_Products extends RecyclerView.Adapter<Adapter_Products.ProductHolder> {
    Context mContext;
    List<product> mProducts;
    VectRay vr;

    IconDrawable icn_rupee;
    PayToken mtoken;

    public Adapter_Products(List<product> products, Context context, PayToken token) {
        mContext = context;
        mProducts = products;
        vr = new VectRay(context, context.getPackageName());
        icn_rupee = new IconDrawable(context, vr.get_icon("{fa} {fa_rupee}"));
        icn_rupee.sizeDp(40);
        icn_rupee.colorRes(R.color.Emrald_green);
        mtoken = token;
    }

    @Override
    public ProductHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.productitems_recy, parent, false);
        ProductHolder pvh = new ProductHolder(v);

        return pvh;
    }


    @Override
    public int getItemCount() {
        return mProducts.size();
    }

    @Override
    public void onBindViewHolder(final ProductHolder holder, final int position) {
        holder.ProducTName.setText(mProducts.get(position).getName());
        holder.price.setText("" + mProducts.get(position).getPrice());
        holder.rupee.setImageDrawable(icn_rupee);
        holder.order_footer_fliper.setOutAnimation(mContext, R.anim.push_left_out);
        holder.order_footer_fliper.setInAnimation(mContext, R.anim.push_right_in);
        if (mProducts.get(position).getOrderStatus() != 0) {
            switch (mProducts.get(position).getOrderStatus()) {
                case 5:
                   /*PENDING*/
                    holder.order_footer_fliper.setDisplayedChild(1);
                    break;
                case 6:
                    /*SUCCESS*/
                    holder.order_footer_fliper.setDisplayedChild(2);
                    break;
                case 7:
                    /*FAILED*/
                    holder.order_footer_fliper.setDisplayedChild(3);
                    break;
               default:
                   holder.order_footer_fliper.setDisplayedChild(0);
                   break;

            }
        }
        Picasso.with(mContext)
                .load(Configs.getServer(mContext) + "/product/" + mProducts.get(position).getProductpic())
                .into(holder.productImg);
        holder.order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowOrderDialogue(position, holder);

            }
        });


    }

    private void ShowOrderDialogue(final int position, final ProductHolder holder) {
        final Dialog dialog = new Dialog(mContext);
        dialog.setContentView(R.layout.order_chooser);
        dialog.setTitle("Complete order");
        TextView productName = (TextView) dialog.findViewById(R.id.ProducTName);
        productName.setText(mProducts.get(position).getName());
        Button place = (Button) dialog.findViewById(R.id.btn_place_order);
        final TextView price = (TextView) dialog.findViewById(R.id.price);
        price.setText("" + mProducts.get(position).getPrice());
        ImageView rupee = (ImageView) dialog.findViewById(R.id.rupee_image);
        rupee.setImageDrawable(icn_rupee);
        ImageView productImage = (ImageView) dialog.findViewById(R.id.product_thumbnail);
        productImage.setImageDrawable(holder.productImg.getDrawable());
        final EditText qty = (EditText) dialog.findViewById(R.id.edt_qty);
        final EditText edt_msg = (EditText) dialog.findViewById(R.id.edt_msg);
        qty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().length() >= 1) {
                    int data = Integer.parseInt(s.toString());
                    if (data > 1) {
                        price.setText((mProducts.get(position).getPrice()) * data + "");
                    } else {
                        price.setText("" + mProducts.get(position).getPrice());

                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        place.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                holder.order.setClickable(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    holder.order.setBackground(mContext.getResources().getDrawable(R.drawable.border_btn_disabled));
                }
                Log.i("ADAPTER", "GOT PLACE CLICK");
                String qtyString, msg;
                qtyString = qty.getText().toString();
                if (qtyString.isEmpty() || qtyString.length() < 1) {
                    qtyString = "1";
                }

                msg = edt_msg.getText().toString();
                JSONObject object = new JSONObject();
                try {
                    Businstance.getInstance().post(new EventPayloads(mProducts.get(position), "ORDER_LOAD"));
                    object.put("storeID", mtoken.getStoreId());
                    object.put("productID", mProducts.get(position).getId());
                    object.put("qty", qtyString);
                    object.put("msg", msg);
                    Log.i("ADAPTER", object.toString());
                    if (SocketCtrl.getInstance().IsConnected)
                        SocketCtrl.getInstance().EmitEvent(Events.EVT_ORDER, object);
                } catch (JSONException e) {
                    e.printStackTrace();

                }
                holder.order_footer_fliper.setDisplayedChild(1);
                mProducts.get(position).setOrderStatus(product.ORDER_STATUS_PENDING);
                dialog.dismiss();

            }
        });
        dialog.show();

    }

    public static class ProductHolder extends RecyclerView.ViewHolder {
        TextView ProducTName, price;
        ImageView productImg, rupee;
        Button order, detail;
        ViewFlipper order_footer_fliper;

        ProductHolder(View itemView) {
            super(itemView);
            ProducTName = (TextView) itemView.findViewById(R.id.ProducTName);
            price = (TextView) itemView.findViewById(R.id.price);
            detail = (Button) itemView.findViewById(R.id.btn_product_detail);
            order = (Button) itemView.findViewById(R.id.btn_product_order);
            productImg = (ImageView) itemView.findViewById(R.id.product_thumbnail);
            rupee = (ImageView) itemView.findViewById(R.id.rupee_image);
            order_footer_fliper = (ViewFlipper) itemView.findViewById(R.id.order_footer_fliper);

        }
    }


}
