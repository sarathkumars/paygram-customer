package com.paygramproject.paygram.ui;

import android.animation.Animator;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.d_codepages.sarathkumar.vectray.VectRay;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.ui.Scanners.NfcScanner;
import com.paygramproject.paygram.ui.Scanners.QRScanner;

import at.markushi.ui.CircleButton;
import butterknife.Bind;

public class FabOptionActiviy extends AppCompatActivity {
    @Bind(R.id.fabViewLayout)    RelativeLayout fabViewLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fab_option_activiy);
        setTitle("");
        fabViewLayout = (RelativeLayout) findViewById(R.id.fabViewLayout);
 //        overridePendingTransition(R.anim.activity_open_translate,R.anim.push_up_out);
        CircleButton nfc = (CircleButton) findViewById(R.id.nfcButton);
        CircleButton qr = (CircleButton) findViewById(R.id.Qrbutton);
        nfc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(FabOptionActiviy.this, NfcScanner.class);
                startActivity(in);
                finish();
            }
        });
        qr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(FabOptionActiviy.this, QRScanner.class);
                startActivity(in);
                finish();

            }
        });
        fabViewLayout.post(new Runnable() {
                               @Override
                               public void run() {
                                   if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

                                     int cx = (fabViewLayout.getLeft() + fabViewLayout.getRight());
                                       int cy = (fabViewLayout.getTop() + fabViewLayout.getBottom()) / 2;
                                       int startradius = 0;
                                       int endradius = Math.max(fabViewLayout.getWidth(), fabViewLayout.getHeight());

                                       Animator animator = ViewAnimationUtils.createCircularReveal(fabViewLayout, cx, cy, startradius, endradius);
                                       animator.setInterpolator(new AccelerateDecelerateInterpolator());
                                       animator.setDuration(600);
                                       animator.start();
                                   }
                               }
                           }
        );




        VectRay vr = new VectRay(this, getPackageName());
        vr.LoadCustomTypeface("app", "app.ttf");
        final IconDrawable qricn = new IconDrawable(this, vr.get_icon("{fa} {fa_qrcode}"));
        qricn.color(Color.WHITE);
        qricn.sizeDp(50);
        qr.setImageDrawable(qricn);
        final IconDrawable nfcicn = new IconDrawable(this, vr.get_icon("{app} {NFC}"));
        nfcicn.color(Color.WHITE);
        nfcicn.sizeDp(50);
        nfc.setImageDrawable(nfcicn);

    }

    @Override
    public void onBackPressed() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {

            int cx = (fabViewLayout.getLeft() + fabViewLayout.getRight());
            int cy = (fabViewLayout.getTop() + fabViewLayout.getBottom())/2;

            int reverse_startradius = Math.max(fabViewLayout.getWidth(), fabViewLayout.getHeight());
            int reverse_endradius=0;

            Animator animate = ViewAnimationUtils.createCircularReveal(fabViewLayout,cx,cy,reverse_startradius,reverse_endradius);
            animate.start();
            animate.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                   finish();

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }else {
            super.onBackPressed();

        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }
}
