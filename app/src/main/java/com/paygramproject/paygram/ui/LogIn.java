package com.paygramproject.paygram.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.d_codepages.sarathkumar.vectray.VectRay;
import com.github.orangegangsters.lollipin.lib.managers.AppLock;
import com.greenfrvr.rubberloader.RubberLoaderView;
import com.greenfrvr.rubberloader.interpolator.PulseInverseInterpolator;
import com.paygramproject.paygram.Api.CustomerApi;
import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Helper.Utils;
import com.paygramproject.paygram.Models.Customer;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Realm.KeyValueStore;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

public class LogIn extends AppCompatActivity {

    @Bind(R.id.Login)
    Button Login;
    @Bind(R.id.btn_SetUp)
    Button btn_SetUp;
    @Bind(R.id.btn_done)
    Button btn_Done;
    @Bind(R.id.email)
    EditText email;
    @Bind(R.id.password)
    EditText password;
    @Bind(R.id.AppIcon)
    ImageView appicon;
    @Bind(R.id.RvView)
    RubberLoaderView loaderView1;
    @Bind(R.id.ViewFLIP)
    ViewFlipper ViewFLIP;
    @Bind(R.id.txview_signup)
    TextView txview_signup;

    private Animation pushDown, pushup;
    private static int REQUEST_CODE_ENABLE = 1015;
    private String AcccessToken = null;
    private Customer ResponseCustomer = null;
    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_right_out);
        setContentView(R.layout.activity_log_in);
        ButterKnife.bind(this);
        pushDown = AnimationUtils.loadAnimation(LogIn.this, R.anim.push_down_out);
        pushup = AnimationUtils.loadAnimation(LogIn.this, R.anim.push_up_in);
        VectRay vr = new VectRay(this, getPackageName());
        vr.LoadCustomTypeface("app", "app.ttf");
        IconDrawable IconPaygram = new IconDrawable(this, vr.get_icon("{app} {AppIcon}"));
        IconPaygram.color(Color.WHITE);
        IconPaygram.sizeDp(100);
        appicon.setImageDrawable(IconPaygram);
        loaderView1.startLoading();
        loaderView1.setInterpolator(new PulseInverseInterpolator());
        loaderView1.startLoading();
        ViewFLIP.setOutAnimation(this, R.anim.push_left_out);
        ViewFLIP.setInAnimation(this, R.anim.push_right_in);

        Login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                loaderView1.setPaletteRes(R.color.mt_cyan, R.color.mt_cyan_drk);
                ViewFLIP.showNext();
                RestAdapter adapter = new RestAdapter.Builder()
                        .setEndpoint(Configs.getServer(getApplicationContext()))
                        .build();
                CustomerApi customer = adapter.create(CustomerApi.class);
                modelBody body = new modelBody();
                body.Email = email.getText().toString();
                body.password = password.getText().toString();
                customer.Login(body, new Callback<Customer>() {
                    @Override
                    public void success(Customer customer, Response response) {
                        txview_signup.setVisibility(View.INVISIBLE);
                        ResponseCustomer = customer;
                        ViewFLIP.showNext();
                        btn_SetUp.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(LogIn.this, LockScreen.class);
                                intent.putExtra(AppLock.EXTRA_TYPE, AppLock.ENABLE_PINLOCK);
                                startActivityForResult(intent, REQUEST_CODE_ENABLE);
                            }
                        });
                        List<Header> headerList = response.getHeaders();
                        for (Header header : headerList) {
                            if (header.getName().equals("AcNtkn")) {
                                AcccessToken = header.getValue();
                            }
                        }

                    }

                    @Override
                    public void failure(RetrofitError error) {
                        loaderView1.setPaletteRes(R.color.flat_red, R.color.flat_red_dark);

                        if (error.getKind().equals(RetrofitError.Kind.HTTP)) {
                            if (error.getResponse().getStatus() == 401)
                                Snackbar.make(v, "Password Or username Error", Snackbar.LENGTH_SHORT).show();
                        } else if (error.getKind().equals(RetrofitError.Kind.NETWORK)) {
                            Snackbar.make(v, "Network Error...", Snackbar.LENGTH_LONG).show();

                        } else if (error.getKind().equals(RetrofitError.Kind.UNEXPECTED)) {
                            Snackbar.make(v, "Unexpected Error...", Snackbar.LENGTH_SHORT).show();

                        }
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                ViewFLIP.showPrevious();
                            }
                        }, 600);

                    }
                });

            }
        });

    }


    public class modelBody {
        public String Email;
        public String password;
    }

    private void writeDb(Customer customer, String token) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(Utils.RealmConverter(customer));
        realm.commitTransaction();

    }

    private void RealmInit(Customer customer, String token) {
        Utils.createNewKeys(Configs.ALIAS, this);
        byte[] key = new byte[25];
        new SecureRandom().nextBytes(key);
        String ourKey = Arrays.toString(key);
        byte[] k = Arrays.copyOfRange(Base64.encode(ourKey.getBytes(), Base64.DEFAULT), 0, 64);
        Utils.fileCreate(this, Configs.RECORDS_FILE, Utils.encryptString(Configs.ALIAS, ourKey, this));
        realm = Utils.SecureRealm(this, k);
        writeDb(customer, token);
        Utils.fileCreate(this, Configs.JWT_CRYPT, Utils.encryptString(Configs.ALIAS, token, this));
        realm.close();
        realm = Utils.DefaultRealmConfigs(this);
        KeyValueStore Store = new KeyValueStore();
        Store.setKey("frun");
        Store.setValue("true");
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(Store);
        realm.commitTransaction();
        realm.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ENABLE) {
            if (AcccessToken != null && ResponseCustomer != null) {
                RealmInit(ResponseCustomer, AcccessToken);
                ViewFLIP.showNext();
                btn_Done.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(LogIn.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        }

    }
}
