package com.paygramproject.paygram.ui.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.d_codepages.sarathkumar.vectray.VectRay;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Socket.Businstance;
import com.paygramproject.paygram.Socket.EventPayloads;
import com.paygramproject.paygram.Socket.Events;
import com.paygramproject.paygram.Socket.SocketCtrl;
import com.paygramproject.paygram.ui.Scanners.NfcScanner;
import com.paygramproject.paygram.ui.Scanners.QRScanner;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sarath Kumar on 05-03-2016.
 */
public class dashboard extends Fragment implements View.OnClickListener {
    View MainView;
    TextView balance;
    FloatingActionButton fab_qr, fab_cash, fab_nfc;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Businstance.getInstance().register(this);
        MainView = inflater.inflate(R.layout.activity_main, container, false);
        VectRay vr = new VectRay(getActivity(), getActivity().getPackageName());
        vr.LoadCustomTypeface("app", "app.ttf");
        FloatingActionsMenu fab = (FloatingActionsMenu) MainView.findViewById(R.id.fab_menu);
        IconDrawable ic_qr = new IconDrawable(getActivity(), vr.get_icon("{fa} {fa_qrcode}"));
        IconDrawable ic_nfc = new IconDrawable(getActivity(), vr.get_icon("{app} {NFC}"));
        IconDrawable ic_cash = new IconDrawable(getActivity(), vr.get_icon("{fa} {fa_rupee}"));
        fab_qr = (FloatingActionButton) MainView.findViewById(R.id.fab_qr);
        fab_nfc = (FloatingActionButton) MainView.findViewById(R.id.fab_Nfc);
        fab_cash = (FloatingActionButton) MainView.findViewById(R.id.fab_cash);
        ic_qr.color(Color.WHITE);
        ic_nfc.color(Color.WHITE);
        ic_cash.color(Color.WHITE);
        ic_qr.sizeDp(22);
        ic_nfc.sizeDp(22);
        ic_cash.sizeDp(22);
        fab_qr.setImageDrawable(ic_qr);
        fab_nfc.setImageDrawable(ic_nfc);
        fab_cash.setImageDrawable(ic_cash);
        fab_qr.setOnClickListener(this);
        fab_nfc.setOnClickListener(this);
        fab_cash.setOnClickListener(this);

        balance = (TextView) MainView.findViewById(R.id.balance);
        balance.setText("5200");
        balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Clicked", Toast.LENGTH_SHORT).show();
                try {
                    SocketCtrl.getInstance().EmitEvent(Events.EVT_CHECKS, new JSONObject().put("request", "BALANCE"));
                    Toast.makeText(getActivity(), "Sends", Toast.LENGTH_SHORT).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "Shits", Toast.LENGTH_SHORT).show();

                }

            }
        });
        TextView toolbar_title = (TextView) MainView.findViewById(R.id.toolbar_title);
        toolbar_title.setText("Wallet");

        ImageView rupee_icon = (ImageView) MainView.findViewById(R.id.ruppe_icon);
        IconDrawable rupee = new IconDrawable(getActivity(), vr.get_icon("{fa} {fa_rupee}"));
        rupee.setShadow(10, 5, 10, Color.parseColor("#7c7c7c"));
        rupee.colorRes(R.color.cloud_grey);
        rupee.sizeDp(55);
        rupee_icon.setImageDrawable(rupee);
        return MainView;
    }

    @Subscribe
    public void update(EventPayloads eventPayloads) {
        if (eventPayloads.getEvent().equals(Events.EVT_CHECKS_REPLY)) {
            Log.i("MAIN", "GOT REPLY");
            JSONObject object = (JSONObject) eventPayloads.getObjct();
            try {
                balance.setText(object.getString("Balance"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fab_qr:{
                Intent in = new Intent(getActivity(), QRScanner.class);
                startActivity(in);
                break;

            }
            case R.id.fab_Nfc:{
                Intent in = new Intent(getActivity(), NfcScanner.class);
                startActivity(in);
                break;

            }
        }
    }
}
