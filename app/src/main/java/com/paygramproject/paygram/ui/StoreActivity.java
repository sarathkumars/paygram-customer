package com.paygramproject.paygram.ui;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.paygramproject.paygram.Models.PayToken;
import com.paygramproject.paygram.Models.product;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Socket.Businstance;
import com.paygramproject.paygram.Socket.EventPayloads;
import com.paygramproject.paygram.ui.StoreFragment.Connection_fragment;
import com.paygramproject.paygram.ui.StoreFragment.bills_fragment;
import com.paygramproject.paygram.ui.StoreFragment.products_fragment;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class StoreActivity extends AppCompatActivity {
    public ViewPager viewPager;
    public static int HIDE_VIEWPAGER = 5;
    public static int SHOW_VIEWPAGER = 10;
    public SmartTabLayout viewpagertab;
    Bundle bundle;
    public List<product> products;
    public List<product> orderList;

    public List<product> getOrderList() {
        if (this.orderList != null) {
            Log.i("STOREACTIVITY_GETORDER", this.orderList.size() + "");
        }
        return orderList;
    }

    @Subscribe
    public void setOrderList(EventPayloads payloads) {
        if (payloads.getEvent().equals("ORDER_LOAD")) {
            if (this.orderList == null) {
                this.orderList = new ArrayList<>();
                this.orderList.add((product) payloads.getObjct());
            } else {

                this.orderList.add((product) payloads.getObjct());
            }
        }

    }

    public List<product> getProducts() {
        return products;
    }

    public void setProducts(List<product> products) {
        this.products = products;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Businstance.getInstance().register(this);
        bundle = null;
        setContentView(R.layout.activity_store);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FragmentPagerItemAdapter adapter;
        PayToken token = getIntent().getParcelableExtra("Token");
        if (token.isTable_service())
        {
            adapter = new FragmentPagerItemAdapter(
                    getSupportFragmentManager(), FragmentPagerItems.with(this)
                    .add(R.string.Connect, Connection_fragment.class)
                    .add(R.string.Store_Products, products_fragment.class)
                    .add(R.string.Payment, bills_fragment.class)
                    .create());
         } else {
            adapter = new FragmentPagerItemAdapter(
                    getSupportFragmentManager(), FragmentPagerItems.with(this)
                    .add(R.string.Connect, Connection_fragment.class)
                    .add(R.string.Payment, bills_fragment.class)
                    .create());
        }
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewpagertab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPager.setAdapter(adapter);
        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }

    public void Putinstance(JSONObject paytoken) throws JSONException {
        bundle = new Bundle();
        bundle.putString("token", paytoken.getString("token"));
    }

    public JSONObject getInstance() {
        JSONObject object = new JSONObject();
        if (bundle == null) {
            return null;
        }
        try {
            object.put("token", bundle.getString("token"));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        if (object.has("token")) {
            return object;
        } else {
            return null;

        }
    }

    @Subscribe
    public void hide_ViewPager(JSONObject CODE) {
        int falg = 0;
        try {
            falg = CODE.getInt("CODE");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i("GOT EVT", falg + "");

        if (falg == HIDE_VIEWPAGER) {

            viewpagertab.setVisibility(View.GONE);

        } else if (falg == SHOW_VIEWPAGER) {
            viewpagertab.setVisibility(View.VISIBLE);

        } else if (falg == SHOW_VIEWPAGER) {

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
