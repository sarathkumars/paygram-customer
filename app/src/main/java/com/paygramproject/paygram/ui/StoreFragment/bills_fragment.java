package com.paygramproject.paygram.ui.StoreFragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paygramproject.paygram.R;

/**
 * Created by Sarath Kumar on 06-02-2016.
 */
public class bills_fragment extends Fragment {

    View MainView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        MainView = inflater.inflate(R.layout.fragment_bills, container, false);
        return  MainView;

    }


}
