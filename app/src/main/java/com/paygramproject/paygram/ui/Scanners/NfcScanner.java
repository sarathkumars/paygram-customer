package com.paygramproject.paygram.ui.Scanners;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.d_codepages.sarathkumar.vectray.VectRay;
import com.d_codepages.sarathkumar.vectray.VectrayDrawable;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Helper.JSON_utils;
import com.paygramproject.paygram.Helper.Utils;
import com.paygramproject.paygram.Models.PayToken;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Socket.Businstance;
import com.paygramproject.paygram.Socket.EventPayloads;
import com.paygramproject.paygram.Socket.Events;
import com.paygramproject.paygram.Socket.SocketCtrl;
import com.paygramproject.paygram.ui.PaymentProcessor;
import com.paygramproject.paygram.ui.views.rippleView;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;
import org.ndeftools.Message;
import org.ndeftools.Record;
import org.ndeftools.util.activity.NfcReaderActivity;
import org.ndeftools.wellknown.TextRecord;

public class NfcScanner extends NfcReaderActivity {

    private static final String TAG = NfcScanner.class.getName();
    protected Message message;
    private rippleView rippleBackground;
    ImageView scancenter, HOLDER;
    VectRay vr;
    private String lastcode;
    private MediaPlayer mediaPlayer;
    @VectrayDrawable(Icon = "{app} {Bulb}", Color = Color.GRAY, Dp = 80)
    public IconDrawable Bulb;
    @VectrayDrawable(Icon = "{app} {NFC}", Color = Color.WHITE, Dp = 80)
    public IconDrawable NFCdrawble;
    @VectrayDrawable(Icon = "{app} {reload}", Color = Color.WHITE, Dp = 80)
    public IconDrawable Reload;
    FloatingActionButton fab;
    FrameLayout InfoCardFrame;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vr = new VectRay(this, getPackageName());
        vr.LoadCustomTypeface("app", "app.ttf");
        setContentView(R.layout.activity_nfc_scanner);
        Businstance.getInstance().register(this);

        mediaPlayer = MediaPlayer.create(this, R.raw.beep);
        InfoCardFrame = (FrameLayout) findViewById(R.id.MainFrame);
        rippleBackground = (rippleView) findViewById(R.id.rippleNfc);
        scancenter = (ImageView) findViewById(R.id.scancenter);
        scancenter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDetecting(true);

            }
        });
        setDetecting(true);
         scancenter.setImageDrawable(NFCdrawble);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        IconDrawable fabdrw = new IconDrawable(this, vr.get_icon("{fa} {fa_times}"));
        fabdrw.color(Color.WHITE);
        fab.setImageDrawable(fabdrw);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (nfcEnabled) {
            rippleBackground.startRippleAnimation();
        } else {
            AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
            alertbox.setTitle("Info");
            alertbox.setMessage("please Turn On Your NFC");
            alertbox.setPositiveButton("Turn On", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                        startActivity(intent);
                    }
                }
            });
            alertbox.setNegativeButton("Close", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            alertbox.show();
        }


     }

    @Override
    public void setDetecting(boolean detecting) {
        super.setDetecting(detecting);
        if (detecting) {
            rippleBackground.startRippleAnimation();
            IconDrawable drawable;
            scancenter.setImageDrawable(NFCdrawble);

        } else {
            rippleBackground.stopRippleAnimation();

        }
    }

    @Subscribe
    public void ValidationCallback(EventPayloads event) {
        setDetecting(false);
         /*Event callback when successful token found*/
        Log.i(TAG, "VALIDATE CALLBACK CALLED");
        if (event.getEvent().equals(Events.EVT_VALIDATE_REPLY)) {
          /*  getting JSON object and parsing for validation*/
            JSONObject object = (JSONObject) (event.getObjct());
            PayToken payToken = null;
            try {
                payToken = JSON_utils.payment_Token_Parser(object);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.i(TAG, "JSON EXCP" + e.getMessage());
            }

            if (payToken != null) {
                Log.i(TAG, "TOKEN NULL");

                if (payToken.isvalid()) {
                    /*token is valid */
                    AnimateScanViewState(true);
                    Log.i(TAG, "VALID TOKEN");
                  /*  checking whether token is Dynamic or Static.because both require Expiration*/
                    if (payToken.getTokentype().equals("Dynamic") || payToken.getTokentype().equals("Static")) {
                        Log.i(TAG, "DYNIMIC || STATIC");
                  /* getting current UNIX timestamp and comparing with token Expire value*/
                        Long tsLong = System.currentTimeMillis();
                        Log.i(TAG, "TIME:" + tsLong);
                        Log.i(TAG, "EXP:" + payToken.getExpire());
                        if(payToken.getStoreId()!=0){
                            infoCardInfalator(payToken);
                        }else
                        if (payToken.ispaid()) {
                            /*checking the token is paid or not*/
                            Log.i(TAG, "ALREADY PAID");

                            Snackbar snackbar = Snackbar
                                    .make(fab, "This Token is already paid!...", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        } else if (tsLong > payToken.getExpire()) {
                       /*     if current timestamp is less than token timestamp that means the token is expired
                            and a snackbar msg will be shown to user*/
                            Log.i(TAG, "EXPIRED");
                            Snackbar snackbar = Snackbar
                                    .make(fab, "Token Expire!...", Snackbar.LENGTH_LONG);
                            snackbar.show();
                        } else {
                          /*  at this point the token validation is complete and proceeding to show token info to the user*/
                            Log.i(TAG, "EVERYTHING GOOD");
                            infoCardInfalator(payToken);

                        }

                    } else if (payToken.getTokentype().equals("Donate")) {
                        Log.i(TAG, "DONATION SCHEME");
                        /*found Donation token this require additional user input from the user*/
                    }

                } else {
                    /*showing invalid token in scanview*/
                    Log.i(TAG, "NON VALID TOKEN");
                    Snackbar snackbar = Snackbar
                            .make(fab, "Invalid Token please try again!...", Snackbar.LENGTH_LONG);
                    snackbar.show();
                    setDetecting(true);
                    AnimateScanViewState(false);
                }
            }
        }
    }


    private void infoCardInfalator(final PayToken paytoken) {
        /*inflator  service for inflating info cardview*/
        LayoutInflater inflater = (LayoutInflater) NfcScanner.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
         /*inflating layout from resource*/
        RelativeLayout validateview = (RelativeLayout) inflater.inflate(R.layout.part_validation_card, null);
        CircularImageView Payee= (CircularImageView) validateview.findViewById(R.id.payeeProf);
        TextView Payee_name = (TextView) validateview.findViewById(R.id.merchantName);
        Button Paybutton = (Button)validateview.findViewById(R.id.Paybutton);
         TextView Amount = (TextView) validateview.findViewById(R.id.Amount);
        Button visit = (Button)validateview.findViewById(R.id.visit);
        String profile_URL = "/store/"+paytoken.getProfile();
        Log.i(TAG, profile_URL);
         if (paytoken.getStoreId() == 0)
         {
             visit.setVisibility(View.GONE);
             ViewGroup.LayoutParams params = Paybutton.getLayoutParams();
             params.width = ViewGroup.LayoutParams.MATCH_PARENT;
             Paybutton.setLayoutParams(params);
             profile_URL = "/profile/"+paytoken.getProfile();
         }else {
             ViewGroup.LayoutParams params = Paybutton.getLayoutParams();
             params.width = ViewGroup.LayoutParams.WRAP_CONTENT;
             Paybutton.setLayoutParams(params);
             visit.setVisibility(View.VISIBLE);
             Amount.setVisibility(View.GONE);
         }
        Picasso.with(this)
                .load(Configs.SERVER + profile_URL)
                .resize(200, 200)
                .centerCrop()
                .into(Payee);
        Payee_name.setText(paytoken.getMerchantName());
        Amount.setText("Rs."+paytoken.getAmount());
        validateview.startAnimation(AnimationUtils.loadAnimation(NfcScanner.this, R.anim.push_up_in));
        InfoCardFrame.removeAllViews();

        Paybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent PaymentProccess = new Intent(NfcScanner.this, PaymentProcessor.class);
                PaymentProccess.putExtra("Token", paytoken);
                startActivity(PaymentProccess);
            }
        });
        visit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SocketCtrl.getInstance().IsConnected)
                {
                    Log.i(TAG, "CONNECTED TRUE");
                    JSONObject object = new JSONObject();
                    try {
                        object.put("storeID",1);
                        object.put("tokenID",paytoken.getId());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    SocketCtrl.getInstance().EmitEvent(Events.EVT_JOIN, object);
                }
            }
        });
        InfoCardFrame.addView(validateview);

    }



    public void AnimateScanViewState(Boolean isSuccess) {
        rippleBackground.stopRippleAnimation();
        final IconDrawable ChangeDrawable;
        if (isSuccess) {
            ChangeDrawable = new IconDrawable(this, vr.get_icon("{fa} {fa_check_circle}"));
            ChangeDrawable.color(Color.WHITE);
            ChangeDrawable.sizeDp(60);
        } else {
            ChangeDrawable = new IconDrawable(this, vr.get_icon("{fa} {fa_times_circle_o}"));
            ChangeDrawable.color(Color.WHITE);
            ChangeDrawable.sizeDp(60);
        }
        rippleBackground.stopRippleAnimation();
        Animation fade_out = Utils.GetXmlAnimation(this, R.anim.fadeout);
        final Animation fade_in = Utils.GetXmlAnimation(this, R.anim.fadein);
        fade_out.setDuration(600);
        fade_in.setDuration(600);
        scancenter.startAnimation(fade_out);
        fade_out.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                scancenter.setImageDrawable(ChangeDrawable);
                scancenter.startAnimation(fade_in);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }


    @Override
    public void readNdefMessage(Message message) {
        AnimateScanViewState(true);
        if (message.size() == 1) {
            this.message = message;
            if (message != null) {
                Log.d(TAG, "Found " + message.size() + " NDEF records");
                Record record = message.get(0);
                if (record instanceof TextRecord) {
                    toast(((TextRecord) record).getText());
                    String rawresult = ((TextRecord) record).getText();
                    if (lastcode == null || !lastcode.equals(rawresult)) {
                        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        vibe.vibrate(100);
                        mediaPlayer.start();

                        JSONObject object = new JSONObject();
                        try {
                            object.put("code", rawresult);
                            Log.i(TAG, "JSON OK");

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.i(TAG, "EMIT OK");

                        SocketCtrl.getInstance().EmitEvent(Events.EVT_CODE_VALIDATE, object);
                        lastcode = rawresult;
                    }

                }
                Log.d(TAG, "READ TEXT " + ((TextRecord) record).getText() + " type " + record.getClass().getSimpleName());
            }
        }

    }


    @Override
    protected void readEmptyNdefMessage() {
        toast(getString(R.string.readEmptyMessage));
        AnimateScanViewState(false);
    }


    @Override
    protected void readNonNdefMessage() {
        toast(getString(R.string.readNonNDEFMessage));
        AnimateScanViewState(false);

    }

    @Override
    protected void onNfcStateEnabled() {

        toast(getString(R.string.nfcAvailableEnabled));
    }

    /**
     * NFC feature was found but is currently disabled
     */

    @Override
    protected void onNfcStateDisabled() {
        toast(getString(R.string.nfcAvailableDisabled));
    }

    /**
     * NFC setting changed since last check. For example, the user enabled NFC in the wireless settings.
     */

    @Override
    protected void onNfcStateChange(boolean enabled) {
        if (enabled) {
            if (rippleBackground != null) {
                if (!rippleBackground.isRippleAnimationRunning()) {
                    rippleBackground.startRippleAnimation();

                }
            }

        } else {
            if (rippleBackground != null) {
                if (rippleBackground.isRippleAnimationRunning()) {
                    rippleBackground.stopRippleAnimation();

                }
            }
        }
    }

    /**
     * This device does not have NFC hardware
     */

    @Override
    protected void onNfcFeatureNotFound() {
        toast(getString(R.string.noNfcMessage));
    }


    public void toast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }

    @Override
    public void onStart() {
        super.onStart();

     }

    @Override
    public void onStop() {
        super.onStop();

    }
}
