package com.paygramproject.paygram.ui;

import android.content.Intent;
import android.os.Bundle;

import com.github.orangegangsters.lollipin.lib.managers.AppLockActivity;
import com.paygramproject.paygram.R;

/**
 * Created by Sarath Kumar on 31-01-2016.
 */
public class Locker extends AppLockActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
    }

    @Override
    public void showForgotDialog() {

    }
    @Override
    public void onPinFailure(int attempts) {
        String REQ = getIntent().getStringExtra("REQ");
        Intent data = new Intent();
        data.putExtra("REQ",REQ);
        data.putExtra("success",false);
        data.putExtra("ATTEM",attempts);
        setResult(RESULT_OK,data);
        finish();
    }
    @Override
    public void onPinSuccess(int attempts) {
        String REQ = getIntent().getStringExtra("REQ");

        Intent data = new Intent();
        data.putExtra("REQ",REQ);
        data.putExtra("success",true);
        data.putExtra("ATTEM",attempts);
        setResult(RESULT_OK,data);
        finish();
    }
}
