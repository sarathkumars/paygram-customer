package com.paygramproject.paygram.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.d_codepages.sarathkumar.vectray.IconDrawable;
import com.d_codepages.sarathkumar.vectray.VectRay;
import com.d_codepages.sarathkumar.vectray.VectrayDrawable;
import com.paygramproject.paygram.Helper.Utils;
import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Realm.KeyValueStore;

import io.realm.Realm;

public class splash extends AppCompatActivity {

    @VectrayDrawable(Icon = "{app} {check}", Color = Color.RED, Dp = 100)
    IconDrawable appicon;
    Realm realm;
    Button SignIn, Signup;
    ImageView icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        VectRay vr = new VectRay(this, getPackageName());
        vr.LoadCustomTypeface("app", "app.ttf");
        SignIn = (Button) findViewById(R.id.btn_signIn);
        Signup = (Button) findViewById(R.id.btn_signUp);
        icon = (ImageView) findViewById(R.id.Appicon);
        SignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(splash.this, LogIn.class);
                in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(in);
                finish();
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (accessControl()) {
            if (Utils.isNetworkOnline(this))
            {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent in = new Intent(splash.this, MainActivity.class);
                        in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(in);
                        finish();
                    }
                }, 100);


            } else {
                Utils.NoNetWorkDialogue(this);
            }

        } else {
            final Animation push_in = Utils.GetXmlAnimation(this, R.anim.push_up_in);
            final Animation push_in2 = Utils.GetXmlAnimation(this, R.anim.push_up_in);
            push_in.setDuration(600);
            push_in2.setDuration(900);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    SignIn.startAnimation(push_in);
                    push_in.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            SignIn.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    push_in2.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            Signup.setVisibility(View.VISIBLE);

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                    Signup.startAnimation(push_in2);
//                    Intent in = new Intent(splash.this, LogIn.class);
//                    in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
////                    startActivity(in);
////                    finish();
                }
            }, 1500);

        }

    }

    private boolean accessControl() {

        try {
            realm = Utils.DefaultRealmConfigs(this);
            KeyValueStore store = realm.where(KeyValueStore.class)
                    .equalTo("Key", "frun")
                    .findFirst();
            if (store.getValue().equals("true")) {
                return true;

            } else {
                Toast.makeText(this, "  NULL", Toast.LENGTH_LONG).show();

                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }


    }

}
