package com.paygramproject.paygram.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.paygramproject.paygram.R;
import com.paygramproject.paygram.Socket.Businstance;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sarath Kumar on 24-02-2016.
 */
public class RecentPayment_Fragment extends Fragment

{
    TextView tx;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        Businstance.getInstance().register(this);
        View view = inflater.inflate(R.layout.fragment_products, container, false);
        return view;

    }
    @Subscribe
    public  void EvenRply(JSONObject obj){
        try {
            tx.setText(obj.getString("done"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
