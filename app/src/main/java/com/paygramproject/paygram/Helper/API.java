package com.paygramproject.paygram.Helper;

/**
 * Created by Sarath Kumar on 17-01-2016.
 */
public class API {
    public static final String API_CUSTOMER_LOGIN = "/api/customer/login";
    public static final String API_CUSTOMER_GET_USER = "/api/customer/";
    public static final String API_CUSTOMER_GET_STORE = "/api/Store/";
    public static final String API_CUSTOMER_GET_PRODUCTS = "/api/products/";
    public static final String API_PAYMENT_PROCESSOR = "/api/dynamic/payment_processor";
}
