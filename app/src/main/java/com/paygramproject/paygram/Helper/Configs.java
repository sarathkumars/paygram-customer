package com.paygramproject.paygram.Helper;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;

/**
 * Created by Sarath Kumar on 17-01-2016.
 */
public class Configs {
    //    public static String SERVER = "http://192.168.1.36:4000";
//     public static String SERVER = "http://192.168.1.35:4000";
     public static String SERVER = "http://192.168.1.33:4000";
//    public static String SERVER = "http://192.168.1.34:4000";
    public static String RECORDS_FILE = "record_data.pgm";
    public static String JWT_CRYPT = "jwtcrypt.pgm";
    public static String ALIAS = "54f2459869c65b935ed48";
    public static String SHARED_PREF_USER = "tempshared";

    public static String getServer(Context con) {
          try{
            File file = new File(Environment.getExternalStorageDirectory(), "Server.tx");
            FileInputStream fin = new FileInputStream(file);
            int c;
            String temp="";
            while( (c = fin.read()) != -1){
                temp = temp + Character.toString((char)c);
            }
             Log.i("STOR",temp);
            return  temp;

        }
        catch(Exception ignored){
            Log.e("STOR",ignored.getMessage());
            return  Configs.SERVER;
        }

    }
}

