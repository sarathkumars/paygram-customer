package com.paygramproject.paygram.Helper;

import android.util.Log;

import com.paygramproject.paygram.Models.PayToken;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Sarath Kumar on 29-01-2016.
 */
public class JSON_utils {

    /*Paytoken parser : used to copy JSON object to Plain old JAVA Object*/
    public  static PayToken payment_Token_Parser(JSONObject object) throws JSONException
    {
        PayToken token =  new PayToken();
        if(object.getBoolean("isvalid")){
            token.setAmount(object.getInt("amount"));
            token.setId(object.getInt("id"));
            token.setExpire(object.getLong("Expire"));
            token.setIspaid(object.getBoolean("ispaid"));
            token.setType(object.getString("type"));
            token.setTokentype(object.getString("Tokentype"));
            Log.i("PARSER", object.getString("Tokentype"));
            token.setTokenHASH(object.getString("TokenHASH"));
            Log.i("PARSER", object.getString("TokenHASH"));
            if(!object.isNull("StoreId")){
                token.setStoreId(object.getInt("StoreId"));
            }
            if(!object.isNull("UserId")){
                token.setUserId(object.getInt("UserId"));
            }
            if(!object.isNull("UseCounter")){
                token.setUseCounter(object.getInt("UseCounter"));
            }
            if(!object.isNull("service")) {
                token.setTable_service(object.getBoolean("service"));

            }
            token.setProfile(object.getString("Profile"));
            token.setMerchantName(object.getString("merchantName"));
            token.setIsvalid(object.getBoolean("isvalid"));
            token.setState(object.getInt("state"));
            return token;
        } else {
            token.setIsvalid(object.getBoolean("isvalid"));
            token.setState(object.getInt("state"));
            return token;
        }
    }


}
