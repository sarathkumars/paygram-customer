package com.paygramproject.paygram;

import android.app.Application;
import android.os.StrictMode;

/**
 * Created by Sarath Kumar on 17-01-2016.
 */
public class Paygram extends Application {



    @Override
    public void onCreate() {
        super.onCreate();
//        PERMISSION Policy
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
     }

}
