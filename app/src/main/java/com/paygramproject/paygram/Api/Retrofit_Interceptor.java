package com.paygramproject.paygram.Api;

import android.content.Context;

import com.paygramproject.paygram.Helper.Configs;
import com.paygramproject.paygram.Helper.Utils;

import retrofit.RequestInterceptor;

/**
 * Created by Sarath Kumar on 22-01-2016.
 */
public class Retrofit_Interceptor {
    private static RequestInterceptor RequestInterceptor;

    public static RequestInterceptor getInstance(Context context) {
        init(context);
        return RequestInterceptor;
    }
     public static void init(Context con) {
        final String token  = Utils.decryptString(Configs.ALIAS,Utils.Fileread(con,Configs.JWT_CRYPT),con);
         RequestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestInterceptor.RequestFacade request) {
                request.addHeader("AcNtkn", token);
            }
        };
    }
    private Retrofit_Interceptor() {
    }
}
