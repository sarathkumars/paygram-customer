package com.paygramproject.paygram.Api;

import com.paygramproject.paygram.Helper.API;
import com.paygramproject.paygram.Models.Customer;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by Sarath Kumar on 17-01-2016.
 */
public interface CustomerApi {

    @GET(API.API_CUSTOMER_GET_USER)
    void getUser(Callback<Customer> CustomerCallback);

    @POST(API.API_CUSTOMER_LOGIN)

    @Headers( "Content-Type: application/json" )
    void Login(@Body Object object,Callback<Customer> CustomerCallback);
}
