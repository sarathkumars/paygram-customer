package com.paygramproject.paygram.Api;

import com.paygramproject.paygram.Helper.API;
import com.paygramproject.paygram.Models.Store;
import com.paygramproject.paygram.Models.product;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by Sarath Kumar on 08-03-2016.
 */
public interface StoreApi {

    @GET(API.API_CUSTOMER_GET_STORE+"{id}")
    void getStore(@Path("id")int storeId,Callback<Store> StoreCallback);

    @GET(API.API_CUSTOMER_GET_PRODUCTS+"{id}")
    void getProducts(@Path("id")int storeId,Callback<List<product>> Products);

}
