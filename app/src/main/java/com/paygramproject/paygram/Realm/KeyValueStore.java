package com.paygramproject.paygram.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Sarath Kumar on 21-01-2016.
 */
public class KeyValueStore extends RealmObject {
    @PrimaryKey
    private String Key;
    private String value;

    public String getKey() {
        return Key;
    }

    public void setKey(String key) {
        Key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
