package com.paygramproject.paygram.Realm;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Sarath Kumar on 17-01-2016.
 */
public class TokenRealm extends RealmObject {
    @PrimaryKey
    private   String Token;

    public String getToken() {
        return Token;
    }

    public void setToken(String token) {
        Token = token;
    }
}
