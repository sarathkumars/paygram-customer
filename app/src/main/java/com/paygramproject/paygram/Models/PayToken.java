package com.paygramproject.paygram.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sarath Kumar on 29-01-2016.
 */
public class PayToken implements Parcelable {
    public int amount;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public int id;
    public boolean table_service;
    public String TokenHASH;

    public boolean isTable_service() {
        return table_service;
    }

    public void setTable_service(boolean table_service) {
        this.table_service = table_service;
    }

    public int UseCounter;
    public int StoreId;
    public long Expire;
    public boolean ispaid;
    public String type;
    public String Tokentype;
    public String createdAt;
    public String merchantName;
    public String getProfile() {
        return Profile;
    }

    public void setProfile(String profile) {
        Profile = profile;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String Profile;
    public int UserId;
    public boolean isvalid;
    public int state;
    public PayToken(){
    }
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public boolean isvalid() {
        return isvalid;
    }

    public void setIsvalid(boolean isvalid) {
        this.isvalid = isvalid;
    }


    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTokenHASH() {
        return TokenHASH;
    }

    public void setTokenHASH(String tokenHASH) {
        TokenHASH = tokenHASH;
    }

    public int getUseCounter() {
        return UseCounter;
    }

    public void setUseCounter(int useCounter) {
        UseCounter = useCounter;
    }

    public int getStoreId() {
        return StoreId;
    }

    public void setStoreId(int storeId) {
        StoreId = storeId;
    }

    public long getExpire() {
        return Expire;
    }

    public void setExpire(long expire) {
        Expire = expire;
    }

    public boolean ispaid() {
        return ispaid;
    }

    public void setIspaid(boolean ispaid) {
        this.ispaid = ispaid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTokentype() {
        return Tokentype;
    }

    public void setTokentype(String tokentype) {
        Tokentype = tokentype;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    private String updatedAt;

    protected PayToken(Parcel in) {
        amount = in.readInt();
        id = in.readInt();
        table_service = in.readByte() != 0x00;
        TokenHASH = in.readString();
        UseCounter = in.readInt();
        StoreId = in.readInt();
        Expire = in.readLong();
        ispaid = in.readByte() != 0x00;
        type = in.readString();
        Tokentype = in.readString();
        createdAt = in.readString();
        merchantName = in.readString();
        Profile = in.readString();
        UserId = in.readInt();
        isvalid = in.readByte() != 0x00;
        state = in.readInt();
        updatedAt = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(amount);
        dest.writeInt(id);
        dest.writeByte((byte) (table_service ? 0x01 : 0x00));
        dest.writeString(TokenHASH);
        dest.writeInt(UseCounter);
        dest.writeInt(StoreId);
        dest.writeLong(Expire);
        dest.writeByte((byte) (ispaid ? 0x01 : 0x00));
        dest.writeString(type);
        dest.writeString(Tokentype);
        dest.writeString(createdAt);
        dest.writeString(merchantName);
        dest.writeString(Profile);
        dest.writeInt(UserId);
        dest.writeByte((byte) (isvalid ? 0x01 : 0x00));
        dest.writeInt(state);
        dest.writeString(updatedAt);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PayToken> CREATOR = new Parcelable.Creator<PayToken>() {
        @Override
        public PayToken createFromParcel(Parcel in) {
            return new PayToken(in);
        }

        @Override
        public PayToken[] newArray(int size) {
            return new PayToken[size];
        }
    };
}