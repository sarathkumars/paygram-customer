package com.paygramproject.paygram.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sarath Kumar on 17-01-2016.
 */
public class Customer implements Parcelable {
    private String Name;
    private String Email;
    private int Balance;
    private String address;

    public String getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(String profilepic) {
        this.profilepic = profilepic;
    }

    private String profilepic;
    private boolean accConfirm;
    private  String Country;
    private String State;
    private int zip,id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String createdAt;
    private  String updatedAt;

    public int getBalance() {
        return Balance;
    }

    public void setBalance(int balance) {
        Balance = balance;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isAccConfirm() {
        return accConfirm;
    }

    public void setAccConfirm(boolean accConfirm) {
        this.accConfirm = accConfirm;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    ;

    protected Customer(Parcel in) {
        Name = in.readString();
        Email = in.readString();
        address = in.readString();
        accConfirm = in.readByte() != 0x00;
        Country = in.readString();
        State = in.readString();
        zip = in.readInt();
        createdAt = in.readString();
        updatedAt = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(Name);
        dest.writeString(Email);
        dest.writeString(address);
        dest.writeByte((byte) (accConfirm ? 0x01 : 0x00));
        dest.writeString(Country);
        dest.writeString(State);
        dest.writeInt(zip);
        dest.writeString(createdAt);
        dest.writeString(updatedAt);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Customer> CREATOR = new Parcelable.Creator<Customer>() {
        @Override
        public Customer createFromParcel(Parcel in) {
            return new Customer(in);
        }

        @Override
        public Customer[] newArray(int size) {
            return new Customer[size];
        }
    };
}
