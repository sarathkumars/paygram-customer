package com.paygramproject.paygram.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sarath Kumar on 08-03-2016.
 */
public class Store implements Parcelable {
    String StoreName;
    int id;
    String StoreDescription;
    String OpenTime;
    String CloseTime;
    String StoreTypes;
    String Address;

    public String getStoreName() {
        return StoreName;
    }

    public void setStoreName(String storeName) {
        StoreName = storeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStoreDescription() {
        return StoreDescription;
    }

    public void setStoreDescription(String storeDescription) {
        StoreDescription = storeDescription;
    }

    public String getOpenTime() {
        return OpenTime;
    }

    public void setOpenTime(String openTime) {
        OpenTime = openTime;
    }

    public String getCloseTime() {
        return CloseTime;
    }

    public void setCloseTime(String closeTime) {
        CloseTime = closeTime;
    }

    public String getStoreTypes() {
        return StoreTypes;
    }

    public void setStoreTypes(String storeTypes) {
        StoreTypes = storeTypes;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getStorePic() {
        return StorePic;
    }

    public void setStorePic(String storePic) {
        StorePic = storePic;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    String StorePic;
    int UserId;


    protected Store(Parcel in) {
        StoreName = in.readString();
        id = in.readInt();
        StoreDescription = in.readString();
        OpenTime = in.readString();
        CloseTime = in.readString();
        StoreTypes = in.readString();
        Address = in.readString();
        StorePic = in.readString();
        UserId = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(StoreName);
        dest.writeInt(id);
        dest.writeString(StoreDescription);
        dest.writeString(OpenTime);
        dest.writeString(CloseTime);
        dest.writeString(StoreTypes);
        dest.writeString(Address);
        dest.writeString(StorePic);
        dest.writeInt(UserId);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Store> CREATOR = new Parcelable.Creator<Store>() {
        @Override
        public Store createFromParcel(Parcel in) {
            return new Store(in);
        }

        @Override
        public Store[] newArray(int size) {
            return new Store[size];
        }
    };
}