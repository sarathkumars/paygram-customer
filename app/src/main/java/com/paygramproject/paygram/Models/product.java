package com.paygramproject.paygram.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Sarath Kumar on 12-03-2016.
 */
public class product implements Parcelable {
    public static  int ORDER_STATUS_PENDING =5;
    public static  int ORDER_STATUS_SUCCESS=6;
    public static  int ORDER_STATUS_FAILED =7;
    int id;
    String Name;
    String Desc;
    float Price;
    int StoreId;
    String createdAt;
    String productpic;
    String updatedAt;
    int orderStatus=0;

    public int getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(int orderStatus) {
        this.orderStatus = orderStatus;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float price) {
        Price = price;
    }

    public int getStoreId() {
        return StoreId;
    }

    public void setStoreId(int storeId) {
        StoreId = storeId;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getProductpic() {
        return productpic;
    }

    public void setProductpic(String productpic) {
        this.productpic = productpic;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public product( ) {
    }



    protected product(Parcel in) {
        id = in.readInt();
        Name = in.readString();
        Desc = in.readString();
        Price = in.readFloat();
        StoreId = in.readInt();
        createdAt = in.readString();
        productpic = in.readString();
        updatedAt = in.readString();
        orderStatus = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(Name);
        dest.writeString(Desc);
        dest.writeFloat(Price);
        dest.writeInt(StoreId);
        dest.writeString(createdAt);
        dest.writeString(productpic);
        dest.writeString(updatedAt);
        dest.writeInt(orderStatus);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<product> CREATOR = new Parcelable.Creator<product>() {
        @Override
        public product createFromParcel(Parcel in) {
            return new product(in);
        }

        @Override
        public product[] newArray(int size) {
            return new product[size];
        }
    };
}